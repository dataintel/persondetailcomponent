import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person-detail',
  template: `<div class="container" *ngFor="let dataPersons of dataPerson">
    <div class="row">
        <div class="col-sm-2">
          <img src="{{dataPersons.image}}" class="img-responsive"/>
        </div>
        <div class="col-sm-5">
          <h3>{{dataPersons.displayname}}</h3>
      Convered in {{dataPersons.counter_news}} has {{dataPersons.counter_statement}}<br>
      Statements.
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
           <h5><b>Appears in</b> : {{dataPersons.related_media}}</h5>
        </div>
    </div>
</div>`,
  styles: ['']
})
export class BaseWidgetPersonDetailComponent implements OnInit {

  public dataPerson: Array<any> = [{"displayname": "Donald Trump","image": "../assets/noimg.png","counter_news": "18,264 news","counter_statement": "8,432",	"related_media": "News Yahoo Singapore (1654), The Times of India (1251), Channel News Asia (994) and more"}];

  constructor() {console.log(this.dataPerson);}
  
  ngOnInit() { 
  }

}
 