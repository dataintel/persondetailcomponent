import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetPersonDetailComponent } from './src/base-widget.component';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
  
  ],
  declarations: [
    BaseWidgetPersonDetailComponent
  ],
  exports: [
   BaseWidgetPersonDetailComponent
  ]
})
export class PersonDetailModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PersonDetailModule,
      providers: []
    };
  }
}
